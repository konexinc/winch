from rot import RotaryEncoder
from gpiozero import PWMOutputDevice, DigitalOutputDevice, Button
import time, zmq

nMot = 4
pas = [0,0,0,0]
pasc = [0,0,0,0]
tpas = [0,0,0,0]
v = [0.0,0.0,0.0,0.0]
mode = ["vit","vit","vit","vit"]
bimmo = [False,False,False,False]

#pinA = [23,8,22,5]
#pinB = [10,6,24,25]
pinA = [5,22,8,23]
pinB = [25,24,6,10]

pinPWM = [17,18,3,14]
pinDir = [15,27,2,4]
#pinEnable = 6

dt = 0.01
K = -0.02
vMax = 0.3
freq = 440

def onCountChange0(pValue):
    global pas, tpas
    i = 0
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue
    
def onCountChange1(pValue):
    global pas, t
    i = 1
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue

def onCountChange2(pValue):
    global pas, t
    i = 2
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue
    
def onCountChange3(pValue):
    global pas, t
    i = 3
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue

    
r = []
for i in range(nMot):
    r.append(RotaryEncoder(pinA[i],pinB[i]))
r[0].when_rotated = onCountChange0
r[1].when_rotated = onCountChange1
r[2].when_rotated = onCountChange2
r[3].when_rotated = onCountChange3

#e = DigitalOutputDevice(pinEnable)

m = []
d = []
for i in range(nMot):
    m.append(PWMOutputDevice(pinPWM[i],frequency = freq))
    d.append(DigitalOutputDevice(pinDir[i]))

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://0.0.0.0:8080")
poll = zmq.Poller()
poll.register(socket,zmq.POLLIN)

# enable drivers
#e.on()
for i in range(nMot):
    d[i].on()
    m[i].value = 0.0



while True:
    # sensors
    t = time.time()
    for i in range(nMot):
        bimmo[i] = False
        if t-tpas[i]>0.1:
            bimmo[i] = True
    # command
    socks = dict(poll.poll(100))
    if socks.get(socket) == zmq.POLLIN:
        rep = socket.recv_json()
        #print rep
        socket.send_json({})
        for repi in rep:
            id = repi["id"]
            if id <=3:
                if repi["ordre"] == "raz":
                    mode[id] = "vit"
                    pas[id] = repi["param"]
                elif repi["ordre"] == "vit":
                    mode[id] = "vit"
                    v[id] = repi["param"]
                elif repi["ordre"] == "pos":
                    mode[id] = "pos"
                    pasc[id] = repi["param"]
            elif id == 4:
                vMax = repi["Vmax"]
                K = repi["K"]
                freq = repi["freq"]
    for i in range(nMot):
        if mode[i] == "pos":
            v[i] = K*(pas[i]-pasc[i])
            if v[i] > vMax :
                v[i] = vMax
            if v[i] < -vMax:
                v[i] = -vMax
            if abs(v[i])<0.05:
                v[i] =0.0
        if v[i] > 0.0 :
            d[i].off()
            m[i].value = v[i]
        elif v[i] == 0.0:
            m[i].value = 0.0
        elif v[i] < 0.0 and bimmo[i] == False:
            d[i].on()
            m[i].value = -v[i]
        elif v[i] < 0.0 and bimmo[i] == True:
            d[i].off()
            m[i].value = -v[i]
    print pas
    time.sleep(dt)



