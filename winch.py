from rot import RotaryEncoder
from gpiozero import PWMOutputDevice, DigitalOutputDevice, Button
import time

pas = 0
pinA = 23
pinB = 24
pinPWM = 15
pinDir = 14
pinEnable = 18
pinBtn1 = 25
pinBtn2 = 8
pinBtn3 = 
dt = 0.1

def onCountChange(pValue):
    global pas 
    pas = pas + pValue

r = RotaryEncoder(pinA,pinB)
r.when_rotated = onCountChange

m = PWMOutputDevice(pinPWM)
d = DigitalOutputDevice(pinDir)
e = DigitalOutputDevice(pinEnable)
btn1 = Button(pinBtn1)
btn2 = Button(pinBtn2)
btn3 = Button(pinBtn3)

e.on()
d.on()
m.value = 0.0
v = 0.0

while True:
    v = 0.0
    if btn1.value == True :
        v = 0.2
    elif btn2.value == True :
        v = -0.2
        
    if v > 0.0 :
        d.on()
        m.value = v
    elif v == 0.0 :
        m.value = 0.0
    elif v < 0.0 :
        d.off()
        m.value = -v
        
    print pas
    time.sleep(dt)




