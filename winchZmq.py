from rot import RotaryEncoder
from gpiozero import PWMOutputDevice, DigitalOutputDevice, Button
import time, zmq

pas = 0
pinA = 23
pinB = 24
pinPWM = 15
pinDir = 14
pinEnable = 18
pinBtn1 = 25
pinBtn2 = 8
dt = 0.1

def onCountChange(pValue):
    global pas 
    pas = pas + pValue

r = RotaryEncoder(pinA,pinB)
r.when_rotated = onCountChange

m = PWMOutputDevice(pinPWM)
d = DigitalOutputDevice(pinDir)
e = DigitalOutputDevice(pinEnable)
btn1 = Button(pinBtn1)
btn2 = Button(pinBtn2)

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://0.0.0.0:8080")
poll = zmq.Poller()
poll.register(socket,zmq.POLLIN)

e.on()
d.on()
m.value = 0.0
v = 0.0
mode = "upDown"
K = 0.04
vMax = 0.8

while True:
    socks = dict(poll.poll(100))
    if socks.get(socket) == zmq.POLLIN:
        rep = socket.recv_json()
        socket.send_json({})
    
        if rep["ordre"] == "raz":
            mode = "upDown"
            pas = 0
        if rep["ordre"] == "up":
            mode = "upDown"
            v = rep["param"]
        if rep["ordre"] == "down":
            mode = "upDown"
            v = - rep["param"]
        if rep["ordre"] == "pos":
            mode = "ctrl"
            
    if mode == "upDown":
        if v > 0.0 :
            d.on()
            m.value = v
        elif v == 0.0 :
            m.value = 0.0
        elif v < 0.0 :
            d.off()
            m.value = -v
    elif mode == "ctrl":
        v = K*(pas-rep["param"])
        if v > vMax :
            v = vMax
        if v < -vMax:
            v = -vMax
            
        if v > 0.0 :
            d.on()
            m.value = v
        elif v == 0.0 :
            m.value = 0.0
        elif v < 0.0 :
            d.off()
            m.value = -v
        
    print pas
    time.sleep(dt)



