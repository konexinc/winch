from rot import RotaryEncoder
from gpiozero import PWMOutputDevice, DigitalOutputDevice, Button
import time, zmq

pas1 = 0
pas2 = 0
pas1c = 0
pas2c = 0
t1=0
t2=0

pinA1 = 8
pinB1 = 7
pinA2 = 1
pinB2 = 12

pinPWM1 = 15
pinDir1 = 14
pinEnable1 = 18

pinPWM2 = 24
pinDir2 = 23
pinEnable2 = 25

dt = 0.01

def onCountChange1(pValue):
    global pas1, t1
    t1 = time.time()
    pas1 = pas1 + pValue
    
def onCountChange2(pValue):
    global pas2, t2
    t2 = time.time()
    pas2 = pas2 + pValue

r1 = RotaryEncoder(pinA1,pinB1)
r1.when_rotated = onCountChange1
r2 = RotaryEncoder(pinA2,pinB2)
r2.when_rotated = onCountChange2

m1 = PWMOutputDevice(pinPWM1)
d1 = DigitalOutputDevice(pinDir1)
e1 = DigitalOutputDevice(pinEnable1)

m2 = PWMOutputDevice(pinPWM2)
d2 = DigitalOutputDevice(pinDir2)
e2 = DigitalOutputDevice(pinEnable2)

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://0.0.0.0:8080")
poll = zmq.Poller()
poll.register(socket,zmq.POLLIN)

e1.on()
e2.on()
d1.on()
d2.on()

m1.value = 0.0
m2.value = 0.0
v1 = 0.0
v2 = 0.0

mode1 = "upDown"
mode2 = "upDown"
K = -0.04
vMax = 0.5

while True:
    
    t = time.time()
    bimmo1 = False
    if t-t1>0.1:
        bimmo1 = True
    bimmo2 = False
    if t-t2>0.1:
        bimmo2 = True
    socks = dict(poll.poll(100))
    if socks.get(socket) == zmq.POLLIN:
        rep = socket.recv_json()
        socket.send_json({})
        if rep["id"] == 1:
            if rep["ordre"] == "raz":
                mode1 = "upDown"
                pas1 = 0
            if rep["ordre"] == "up":
                mode1 = "upDown"
                v1 = rep["param"]
            if rep["ordre"] == "down":
                mode1 = "upDown"
                v1 = - rep["param"]
            if rep["ordre"] == "pos":
                mode1 = "ctrl"
                pas1c = rep["param"]
        if rep["id"] == 2:
            if rep["ordre"] == "raz":
                mode2 = "upDown"
                pas2 = 0
            if rep["ordre"] == "up":
                mode2 = "upDown"
                v2 = rep["param"]
            if rep["ordre"] == "down":
                mode2 = "upDown"
                v2 = - rep["param"]
            if rep["ordre"] == "pos":
                mode2 = "ctrl"
                pas2c = rep["param"]
            
    if mode1 == "ctrl":
        v1 = K*(pas1-pas1c)
        if v1 > vMax :
            v1 = vMax
        if v1 < -vMax:
            v1 = -vMax
    if v1 > 0.0 :
        d1.off()
        m1.value = v1
    elif v1 == 0.0:
        m1.value = 0.0
    elif v1 < 0.0 and bimmo1 == False:
        d1.on()
        m1.value = -v1
    elif v1 < 0.0 and bimmo1 == True:
        d1.off()
        m1.value = -v1
            
    if mode2 == "ctrl":
        v2 = K*(pas2-pas2c)
        if v2 > vMax :
            v2 = vMax
        if v2 < -vMax:
            v2 = -vMax
    if v2 > 0.0 :
        d2.on()
        m2.value = v2
    elif v2 == 0.0 :
        m2.value = 0.0
    elif v2 < 0.0 and bimmo2 == False:
        d2.off()
        m2.value = -v2
    elif v2 < 0.0 and bimmo2 == True:
        d2.on()
        m2.value = -v2
    print pas1,pas2
    print bimmo2
    time.sleep(dt)



